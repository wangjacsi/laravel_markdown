<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel 5 Essential</title>

    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
    @yield('style')
</head>
<body>
    <div class="container">
        @yield('content')

        @include('layouts.footer')
    </div>

        <script src=" {{ elixir('js/app.js') }}"></script>
        @yield('script')

</body>
</html>